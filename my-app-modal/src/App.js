import React from "react";
import Button from "./components/Button";
import Modal from "./components/Modal";
import "./App";


export default class App extends React.PureComponent {
  state = {
    firstModal: false,
    secondModal: false,
  };

  openFirstModal = () => {
    this.setState({ firstModal: true });
  };

  closeFirstModal = () => {
    this.setState({ firstModal: false });
  };

  openSecondModal = () => {
    this.setState({ secondModal: true });
  };

  closeSecondModal = () => {
    this.setState({ secondModal: false });
  };

  render() {
    const { firstModal, secondModal } = this.state;

    return (
      <div className="App">
        <button 
          className="button-window"
          backgroundColor="blue"
          text="Open first modal"
          onClick={this.openFirstModal}
        >First Modal</button>
        <button
          className="button-window"
          backgroundColor="red"
          text="Open second modal"
          onClick={this.openSecondModal}
        >Second Modal</button>

        {firstModal && (
          <Modal
            header="Do you want to delete this file?"
            closeButton={true}
            text="Once you delete this file, it won't be possible to undo this action.
            Are you sure you want to delete it?"
            actions={
              <>
                <Button
                  backgroundColor="#b3382c"
                  text="Ok"
                  onClick={this.closeFirstModal}
                ></Button>
                <Button
                backgroundColor="#b3382c"
                text="Cancel"
                onClick={this.closeFirstModal}
                ></Button>
              </>
            }
            onClose={this.closeFirstModal}
          />
        )}

        {secondModal && (
          <Modal
            header="Hello, i'm second Modal"
            closeButton={true}
            text="This is the second modal window."
            actions={
              <>
              <Button
                  backgroundColor="#b3382c"
                  text="Ok"
                  onClick={this.closeSecondModal}
                ></Button>
                <Button
                backgroundColor="#b3382c"
                text="Cancel"
                onClick={this.closeSecondModal}
                ></Button>
              </>
            }
            onClose={this.closeSecondModal}
          />
        )}
      </div>
    );
  }
}
