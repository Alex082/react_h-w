import React from "react";


export default class Modal extends React.PureComponent {
  clickButton = (event) => {
    if (event.target.classList.contains("modal-wrapper")) {
      this.props.onClose();
    }
  };

  render() {
    const { header, closeButton, text, actions } = this.props;

    return (
      <div className="modal-wrapper" onClick={this.clickButton}>
        <div className="modal">
        <div className="title">
        <h2 className="text-title">{header}</h2>
          {closeButton && (
            <span className="close-button" onClick={this.props.onClose}>
              &times;
            </span>
          )}
        </div>
        <div className="content-modal">
        <p className="text-content">{text}</p>
          <div className="actions">{actions}</div>
        </div>
          
        </div>
      </div>
    );
  }
}
