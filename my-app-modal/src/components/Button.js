import React from "react";


export default class Button extends React.PureComponent {
  render() {
    const { backgroundColor, text, onClick } = this.props;

    return (
      <button
        className="button"
        style={{ backgroundColor: backgroundColor }}
        onClick={onClick}
      >
        {text}
      </button>
    );
  }
}
